import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Login1 from '../Login1';
import Login2 from '../Login2';
import Login3 from '../Login3';

const RootStack = createNativeStackNavigator();
const approuter = () => {
    return (
        <NavigationContainer>

            <RootStack.Navigator>
              
                <RootStack.Screen name="Login1" component={Login1} />
                <RootStack.Screen name="Login2" component={Login2} />
                <RootStack.Screen name="Login3" component={Login3} />
               
            </RootStack.Navigator>

        </NavigationContainer>
    )
}
export default approuter