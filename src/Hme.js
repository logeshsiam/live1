import React from "react";
import { Text, StyleSheet, View, Image, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
import Icon from 'react-native-vector-icons/Fontisto';
import AIcon from 'react-native-vector-icons/AntDesign';
import FIcon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';


const Hme = () => {

  return (

    <ScrollView style={{ flexDirection: "column" }}>

      <SafeAreaView>

        <View>
          <Image source={require('../assets/white.png')} style={styles.white} />
          <Text style={{ textAlign: "right", color: "white", marginRight: 30, marginTop: 20 }}>
            <Icon name="search" size={20} color="#D04C8D" />     <FIcon name="settings" size={22} color="#D04C8D" />
          </Text>

          <Text style={styles.textDesign}>Good Morning ,{'\n'} Angie</Text>
          <View>
            <Image source={require('../assets/l1.png')} style={styles.imageStyle} />
            <Image source={require('../assets/load1.png')} style={styles.load1} />
            <Text style={styles.textDesign3}> 20% {'\n'}</Text>
            <Text style={styles.textDesign4}>Profile Completion</Text>

          </View>

          {/* linearGradient  box*/}

          <View>
            <LinearGradient colors={['#57508D', '#F54BA1']} style={styles.linearGradient}>
              <AIcon name="warning" size={30} color="#000" style={styles.warning} />
              <AIcon name="warning" size={30} color="#000" style={styles.warning1} />
              <Text style={styles.text1}>
                Complete Your Profiling Assessment
              </Text>

              <Text style={styles.text2}>
                For the best experience on using MyNext,{'\n'} complete all pending assessments.
              </Text>
            </LinearGradient>
          </View>

          {/* Assessments */}

          <Text style={styles.textDesign1}>Assessments</Text>
          <Text style={styles.textDesign2}>view all</Text>

          {/* imagebox */}

          <Image source={require('../assets/l3.png')} style={styles.imageStyle2} />
          <Text style={styles.font1}>About Me {'\n'}  </Text><Text style={styles.font2}>View and Manage information  {'\n'} about yourself</Text >
          <AIcon name="checkcircle" size={18} color='#00FF38' style={{ left: 40 }} />
          <Text style={styles.font3}>  100% Completed </Text>

          <View style={{ top: 5 }}>
            <Image source={require('../assets/l4.png')} style={styles.imageStyle2} /><Text style={styles.font1}>Catalyst Profiling {'\n'}  </Text><Text style={styles.font2}>Understand yourself better to help {'\n'} become your own agents
            </Text>
            <Image source={require('../assets/load2.png')} style={styles.load2} />
            <Image source={require('../assets/load3.png')} style={styles.load3} />
            <Text style={styles.font4}> 50% Completed </Text>
          </View>
          <Image source={require('../assets/l5.png')} style={styles.imageStyle2} /><Text style={styles.font1}>Career Explorations {'\n'}  </Text><Text style={styles.font2}>Explore potential careers related to {'\n'} your profiling
          </Text><Image source={require('../assets/load2.png')} style={styles.load4} />
          <Text style={styles.font5}> 0% Completed </Text>

          <Image source={require('../assets/l6.png')} style={styles.imageStyle2} /><Text style={styles.font1}>Employability Factors {'\n'}  </Text><Text style={styles.font2}>Create self-reflection on your available {'\n'} knowledge, skills and abilities.
          </Text><Image source={require('../assets/load2.png')} style={styles.load5} />
          <Text style={styles.font5}> 0% Completed </Text>

          <Image source={require('../assets/l7.png')} style={styles.imageStyle2} /><Text style={styles.font1}>English Proficiency {'\n'}  </Text><Text style={styles.font2}>The online test gives a good indication  {'\n'} of your proficiency level.
          </Text><Image source={require('../assets/load2.png')} style={styles.load6} />
          <Text style={styles.font5}> 0% Completed </Text>

          <Image source={require('../assets/l8.png')} style={styles.imageStyle2} /><Text style={styles.font1}>Basic IT Skills {'\n'}  </Text><Text style={styles.font2}>This online test is intended to measure  {'\n'} your basic knowledge and skils.
          </Text><Image source={require('../assets/load2.png')} style={styles.load7} />
          <Text style={styles.font5}> 0% Completed </Text>

          {/* blogs */}
          <Text style={{ borderBottomWidth: 0.2, width: 330, marginTop: 45, left: 30 }} ></Text>
          <Text style={styles.textDesign1}>Blogs & Updates</Text>
          <Text style={styles.textDesign2}>view all</Text>

          <View style={{ marginTop: 30 }}>

            {/* blog Image */}

            <Image source={require('../assets/blog.png')} style={styles.blog} />
            <View>
              <View colors={'#57508D'} style={styles.blogtext}>

                <Text style={styles.blogtext1}>
                  #Door2Work
                </Text>
                <Text style={styles.blogtext2}>
                  Our main focus is to empower our fellow {'\n'}Malaysians by providing them with opportunities that {'\n'} enable both career and personal growth.
                </Text>
              </View>
            </View>
          </View>

          {/* address */}
          <Image source={require('../assets/Logo.png')} style={styles.Logo} />

          <View>
            <Text style={{ borderBottomWidth: 0.2, width: 330, marginTop: -5, left: 30 }} ></Text>
            <Text style={styles.address}> Locate us:{'\n'}
              6th Floor, Surian Tower, 1, Jalan PJU 7/3, Mutiara Damansara, {'\n'}
              47810 Petaling Jaya, Selangor</Text>
            <Text style={styles.address}>Contact Us    +603 7839 7000 </Text>
          </View>



          <Text style={{ marginTop: 50 }}> </Text>


        </View>


      </SafeAreaView>

    </ScrollView>
  )
}
export default Hme;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 30,
    marginTop: 20,
    marginBottom: 670,
  },
  container1: {
    flex: 1,
    marginLeft: 30,
    marginTop: 20,
    color: '#000'
  },
  container4: {
    flex: 1,
    marginTop: 10,
    justifyContent: 'center',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 20,
    height: 80,
    width: 300,
    marginLeft: 40,
    marginTop: 10,
  },
  rectangle: {
    height: 80,
    width: 300,
    backgroundColor: '#800080',
    marginLeft: 17,
    borderRadius: 20,
  },
  // text
  text1: {
    marginTop: 20,
    fontSize: 13,
    fontFamily: 'Gill Sans',
    fontWeight: "bold",
    textAlign: 'right',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  text2: {
    fontSize: 10,
    fontFamily: 'Gill Sans',
    marginTop: -10,
    marginLeft: 50,
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  vector: {
    alignSelf: 'right',
    marginTop: 10,
    height: 320,
    width: 300,
    right: 12,
  },
  // imagestyle
  imageStyle: {
    alignSelf: 'center',
    marginTop: 10,
    height: 250,
    width: 250,
    right: 12,
  },
  imageStyle1: {
    alignSelf: 'center',
    marginTop: 10,
    height: 90,
    width: 300,
    borderRadius: 20,
    right: 12,
  },
  imageStyle2: {
    alignSelf: 'center',
    marginTop: 25,
    height: 220,
    width: 350,
    left: 0,
    borderRadius: 10,
    right: 12,
  },
  imageStyle3: {
    alignSelf: 'center',
    marginTop: 10,
    height: 250,
    width: 250,
    right: 12,
    borderColor: '#black',
    borderWidth: 10,
  },
  // load
  load1: {
    // position: 'absolute',
    // marginTop: 235,
    marginLeft: 120,
    bottom: 18,
    left: 5

  },
  load2: {
    // position: 'absolute',
    // marginTop: 912,
    marginLeft: 50,
    top: 20
  },
  load3: {
    // position: 'absolute',
    // marginTop: 912,
    marginLeft: 50,
    top: 16
  },
  load4: {
    // position: 'absolute',
    // marginTop: 1153,
    marginLeft: 40,
    top: 20
  },
  load5: {
    // position: 'absolute',
    // marginTop: 1393,
    marginLeft: 40,
    top: 20
  },
  load6: {
    // position: 'absolute',
    // marginTop: 1632,
    marginLeft: 40,
    top: 20
  },
  load7: {
    // position: 'absolute',
    // marginTop: 1872,
    marginLeft: 40,
    top: 20
  },
  // font
  font1: {
    fontSize: 18,
    color: '#fff',
    marginTop: -90,
    fontWeight: '700',
    left: 45,
  },
  font2: {
    fontSize: 11,
    color: '#fff',
    marginTop: -20,
    left: 45,
  },
  font3: {
    fontSize: 10,
    fontWeight: '700',
    color: '#00FF38',
    marginTop: -14,
    left: 55,
  },
  font4: {
    fontSize: 10,
    color: '#00FF38',
    fontWeight: '700',
    marginTop: 5,
    left: 210
  },
  font5: {
    fontSize: 10,
    color: '#ff0000',
    fontWeight: '700',
    marginTop: 12,
    marginLeft: 200,
  },
  // warning
  warning: {
    fontSize: 25,
    color: '#fff',
    position: "absolute",
    marginTop: 17,
    marginLeft: 10,
  },
  warning1: {
    fontSize: 30,
    color: '#fff',
    position: "absolute",
    marginTop: 32,
    marginLeft: 17,
  },
  // textdesign
  textDesign: {
    fontSize: 20,
    color: '#000',
    marginTop: -30,
    marginLeft: 20,
  },
  textDesign1: {
    fontSize: 18,
    color: '#000',
    marginTop: 30,
    marginLeft: 30,
  },
  textDesign2: {
    fontSize: 14,
    color: "#D04C8D",
    marginTop: -30,
    marginLeft: 320,
    textDecorationLine: "underline",
  },
  textDesign3: {
    fontSize: 13,
    color: "#D04C8D",
    marginTop: -30,
    marginLeft: 220,
    fontWeight: "bold"
  },
  textDesign4: {
    fontSize: 10,
    color: "#D04C8D",
    marginTop: -20,
    marginLeft: 200,
  },
  blog: {
    alignSelf: 'center',
    marginTop: 20,
    height: 150,
    width: 300,
    borderRadius: 34,
    left: 4
  },
  blogtext: {
    alignSelf: 'center',
    position: 'absolute',
    marginTop: -29,
    marginRight: 30,
    height: 80,
    width: 300,
    borderBottomLeftRadius: 34,
    borderBottomRightRadius: 34,
    right: 12,
    backgroundColor: '#fff'
  },
  blogtext1: {
    marginTop: 10,
    fontSize: 18,
    fontFamily: 'Gill Sans',
    fontWeight: "bold",
    marginLeft: 20,
    margin: 10,
    color: '#D04C8D',
    backgroundColor: 'transparent',
  },
  blogtext2: {
    fontSize: 10,
    fontFamily: 'Gill Sans',
    marginTop: -10,
    marginLeft: 20,
    margin: 10,
    color: '#000',
    backgroundColor: 'transparent',
  },
  Logo: {
    marginTop: 140,
    left: 30
  },
  address: {
    fontSize: 10,
    marginTop: 10,
    left: 30,
    color: '#9A9A9A'
  },
  white: {
    alignSelf: 'center',
    opacity: 1,
    position: "absolute",
    marginTop: -10,
    borderRadius: 80,
    height: 435,
    width: 450,

  },

})
