import React from "react";
import {Text, StyleSheet} from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";
 const Homepage =()=>{
     return(
         <SafeAreaView style={styles.container}>
             <Text style={styles.textDesign}>HomePage</Text>
         </SafeAreaView>
     )
 }
 export default Homepage;

 const styles = StyleSheet.create({
     container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
     },
     textDesign:{
         fontSize: 30,
         fontWeight: 'bold',
         color:'#0095C6',
     }
 })
