import React,{useState}from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  StyleSheet,
  StatusBar,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { Checkbox } from 'react-native-paper';

const Slider = ({navigation}) => {
  const [sliderState, setSliderState] = useState({ currentPage: 1 });
  const { width, height } = Dimensions.get('window');
  const [checked, setChecked] = React.useState(false);

  const setSliderPage = (event) => {
    const { currentPage } = sliderState;
    const { x } = event.nativeEvent.contentOffset;
    const indexOfNextScreen = Math.floor(x / width);
    if (indexOfNextScreen !== currentPage) {
      setSliderState({
        ...sliderState,
        currentPage: indexOfNextScreen,
      });
    }
  };

  return (
    <>  
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1,alignItems:'center' }}>
        <ScrollView
          horizontal={true}
          scrollEventThrottle={16}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          onScroll={(event) => {
            setSliderPage(event);
          }}
        >
          <View style={{ width, height }}>
            <Text style={styles.Title}>Welcome to Freerides</Text>
            <Image source={require('../assets/Screen1.png')} style={styles.imageStyle} />
            <Text style={styles.TextStyle} >Create A Profile</Text>
            <Text style={{alignSelf:'center',marginTop: 30,fontSize:15}}> Lorem ipsum dolor sit amet, consectetur {'\n'}  adipiscing elit, sed do eiusmod tempor{'\n'}incididunt ut labore et dolore magna aliqua.</Text>
            <Image source={require('../assets/slide1.png')} style={{height: 10, width: 60,alignSelf:'center',marginTop:30}}/>
          </View>

           <View style={{ width, height }}>
            <Text style={styles.Title}>Welcome to Freerides</Text>
            <Image source={require('../assets/Screen2.png')} style={styles.imageStyle} />
            <Text style={styles.TextStyle} >Find & offer Comfortable Ride</Text>
            <Text style={{alignSelf:'center',marginTop: 30,fontSize:15}}> Lorem ipsum dolor sit amet, consectetur {'\n'}  adipiscing elit, sed do eiusmod tempor{'\n'}incididunt ut labore et dolore magna aliqua.</Text>
            <Image source={require('../assets/slide2.png')} style={{height: 10, width: 60,alignSelf:'center',marginTop:30}}/>
          </View>
         
          <View style={{ width, height }}>
            <Text style={styles.Title}>Welcome to Freerides</Text>
            <Image source={require('../assets/Screen3.png')} style={styles.imageStyle} />
            <Text style={styles.TextStyle} >Join the Community</Text>
            <Text style={{alignSelf:'center',marginTop: 30,fontSize:15}}> Lorem ipsum dolor sit amet, consectetur {'\n'}  adipiscing elit, sed do eiusmod tempor{'\n'}incididunt ut labore et dolore magna aliqua.</Text>
            <Image source={require('../assets/slide3.png')} style={{height: 10, width: 60,alignSelf:'center',marginTop:30,marginBottom: 20}}/>
            <View  style={styles.checkBox}>
            <Checkbox
              status={checked ? 'checked' : 'unchecked'}
              onPress={() => {
               setChecked(!checked);
            }}
            size={1}
            color={'#0095C6'}
            uncheckColor={'red'}  
         />
         <Text style={{fontSize: 11,padding:4,alignSelf:'center',marginTop:3}}>
         <Text>I have read all the </Text>
         <Text style={styles.highlight}>Terms and Conditions</Text>
         <Text> and </Text>
         <Text style={styles.highlight}>Privacy Policy</Text> 
         <Text>{'\n'}and Agree to the same</Text> 
          </Text>
         </View>
        <TouchableOpacity style={{marginTop: 30}} onPress={() => navigation.navigate('PhoneNo')}>
        <Text style = {[styles.button,{color:'#fff',textAlign:'center'}]}>Get Started</Text>
        </TouchableOpacity>
        </View>
        </ScrollView>  
        </SafeAreaView>
      </>
  );
};
export default Slider;
const styles = StyleSheet.create({
  imageStyle: {
    alignSelf:'center',
    marginTop: 70 ,
    height: 240,
    width: 290,
  },
  paginationWrapper: {
    position: 'absolute',
    bottom: 170,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  paginationDots: {
    height: 10,
    width: 10,
    borderRadius: 10 / 2,
    backgroundColor: '#0898A0',
    marginLeft: 10,
  },
  Title:{
    marginTop:20,
    marginLeft:20,
    fontWeight:'500',
    fontSize: 24,
    color: '#000'
},
TextStyle:{
    marginTop: 40,
    fontWeight:'600',
    alignSelf:'center',
    fontSize: 20,
    fontWeight:'500',
    color:'#000',
},
checkBox:{
   flexDirection:'row',
   marginHorizontal: 5,
},
highlight:{
  color:'#0095C6',
  textDecorationLine:'underline',  
},
button: {
      borderRadius: 5,
      alignSelf:'center',
      fontSize: 18,
      height:40,
      width:200,
      padding: 6,
      backgroundColor:'#0095C6', 
  },
});
