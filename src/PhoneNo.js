import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
const PhoneNo = ({navigation}) => {
    const [phoneNumber, setphoneNumber] = useState('');
    const phoneInput = useRef(null);
    
    return (
        <View>
            <Text style={styles.Title}>Login</Text>
            <Text style={{ marginTop: 30, fontSize: 16, marginLeft: 15 }}> Enter Your Mobile Number for OTP </Text>
            <View style={styles.container}>
                <PhoneInput style={{padding:2}}
                    ref={phoneInput}
                    defaultValue={phoneNumber}
                    defaultCode="IN"
                    placeholder="Enter valid number"
                    containerStyle={styles.phoneContainer}
                    textContainerStyle={styles.textInput}
                    onChangeFormattedText={text => {
                        setphoneNumber(text);
                    }} />
                    <TouchableOpacity  onPress={() => navigation.navigate('OTP')}>
                    <Text style = {[styles.button,{color:'#fff',textAlign:'center'}]} >Sumbit</Text>
                    </TouchableOpacity>
                
            </View>
        </View>
    );
};
export default PhoneNo;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    phoneContainer: {
        width: '90%',
        height: 40,
        marginTop: '40%',
        borderWidth: 0.5,
    },
   button: {
      borderRadius: 5,
      alignSelf:'center',
      marginTop: 40,
      fontSize: 18,
      height:40,
      width:180,
      padding: 6,
      backgroundColor:'#0095C6',    
  },
    textInput: {
        paddingVertical: -6,
    },
    continueText: {
        color: '#fff',
        marginBottom: 2,
        fontSize: 50,
    },
    Title: {
        marginTop: 20,
        marginLeft: 20,
        fontWeight: '500',
        fontSize: 24,
        color: '#000'
    },
});
